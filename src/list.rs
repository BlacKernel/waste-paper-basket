use crate::parse;

use std::path::Path;
use std::fs;

pub fn list(options: parse::Options, mut mut_options: parse::MutOptions) {
    let dest_dir = Path::new(&options.dest_dir);
    let trashfiles_path = &dest_dir.join("files");

    println!("Trashed files in {}:", &trashfiles_path.display());

    for entry in fs::read_dir(trashfiles_path).unwrap() {
        let entry = entry.unwrap();
        let path  = entry.path();

        let file_name = path
            .file_name()
            .unwrap()
            .to_str()
            .unwrap();

        println!("\t{}", file_name);
    }
}
