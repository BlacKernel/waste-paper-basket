/************************************************************************************
 *                                                                                  *
 * Code blatently stolen from:                                                      *
 * https://github.com/msmith491/rust-cowsay/blob/master/src/main.rs                 *
 *                                                                                  *
 *                                                                                  *
 * MIT License                                                                      *
 *                                                                                  *
 * Copyright (c) 2016 Matthew Smith                                                 *
 *                                                                                  *
 * Permission is hereby granted, free of charge, to any person obtaining a copy     *
 * of this software and associated documentation files (the "Software"), to deal    *
 * in the Software without restriction, including without limitation the rights     *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell        *
 * copies of the Software, and to permit persons to whom the Software is            *
 * furnished to do so, subject to the following conditions:                         *
 *                                                                                  *
 * The above copyright notice and this permission notice shall be included in all   *
 * copies or substantial portions of the Software.                                  *
 *                                                                                  *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR       *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,         *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE      *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER           *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,    *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE    *
 * SOFTWARE.                                                                        *
 *                                                                                  *
 * **********************************************************************************/
static FORTUNES: &str = r#""You cannot buy the revolution. You cannot make the revolution. You can only be the revolution. It is in your spirit, or it is nowhere" - Ursula K. Le Guin, The Dispossessed%"People have only as much liberty as they have interlligence to want and the courage to take" - Emma Goldman%"That is what I have always understood to be the essence of anarchism: the conviction that the burden of proof has to be placed on authority, and that it should be dismantled if that burden cannot be met." - Noam Chomsky%"Our masters have not heard the people's voice for generations and it is much, much louder than they care to remember" - Alan Moore, V for Vendetta%"Ask for work. If they don't give you work, ask for bread. If they do not give you work or bread, then take bread." - Emma Goldman, Anarchism and Other Essays%"To be GOVERNED is to be watched, inspected, spied upon, directed, law-driven, numbered, regulated, enrolled, indoctrinated, preached at, controlled, checked, estimated, valued. censured, commanded, by creatures who have neither the right nor the wisdom nor the virtue to do so. To be GOVERNED is to be at every operation, at every transaction noted, registered, counted, taxed, stamped, measured, numbered, assesed, licensed, authorized, admonished, prevented, forbidden, reformed, corrected, punished. It is, under pretext of public utility, and in the name of general interest, to be placed under contribution, drilled, fleeced, exploited, monopolized, extorted from, squeezed, hoaxed, robbed; then, at the slightest resistance, the first word of complaint, to be repressed, fined, vilified, harassed, hunted down, abused, clubbed, disarmed, bound, choked, imprisoned, judged, condemned, shot, deported, sacrificed, sold, betrayed; and to crown all, mocked, ridiculed, derided, outraged, dishonored. That is government; that is its justice; that is its morality" - Pierre-Joseph Proudhon, General Idea of the Revolution in the Nineteenth Century%"Your pretty empire took so long to build, now, with a snap of history's fingers, down it goes." - Alan Moore, V for Vendetta%"Every society has the criminals it deserves." - Emma Goldman, Red Emma Speaks%"Authority, when first detecting chaos at its heels, will entertain the vilest schemes to save its orderly facade." - Alan Moore, V for Vendetta%"Anarchism is founded on the observaton that since few men are wise enough to rule themselves, even fewer are wise enough to rule others." - Edward Abbey%"At one time in the world there were woods that no one owned" - Cormac McCarthy, Child of God%"The individual cannot bargain with the State. The State recognizes no coinage but power: and it issues the coins itself." - Ursula K. Le Guin, The Dispossessed"#;
static COW: &str = r#"$the_cow = <<"EOC";
        $thoughts   ^__^
         $thoughts  ($eyes)\\_______
            (__)\\       )\\/\\
             $tongue ||----w |
                ||     ||
EOC"#;

use std::str;
use std::env;
use std::io::{self, Read};
use std::fs::File;

use rand::Rng;

struct CowBubble {
    sleft: &'static str,
    sright: &'static str,
    topleft: &'static str,
    midleft: &'static str,
    botleft: &'static str,
    topright: &'static str,
    midright: &'static str,
    botright: &'static str,
}

fn format_animal(s: String, thoughts: &str, eyes: &str, tongue: &str) -> String {
    s.split("\n")
        .filter(|&x| !x.starts_with("##") && !x.contains("EOC"))
        .collect::<Vec<_>>()
        .join("\n")
        .trim_right()
        .replace("$eyes", eyes)
        .replace("$thoughts", thoughts)
        .replace("$tongue", tongue)
        .replace("\\\\", "\\")
        .replace("\\@", "@")
}

fn make_bubble(s: String, width: usize, think: bool, wrap: bool) -> String {
    let mut result = Vec::new();
    let mut top = vec![" "];
    let mut bottom = vec![" "];
    let topc = "_";
    let bottomc = "-";
    let pad = ' ';
    let mut cowb = CowBubble {
        sleft: "<",
        sright: ">",
        topleft: "/",
        midleft: "|",
        botleft: "\\",
        topright: "\\",
        midright: "|",
        botright: "/",
    };

    if think {
        cowb = CowBubble {
            sleft: "(",
            sright: ")",
            topleft: "(",
            midleft: "(",
            botleft: "(",
            topright: ")",
            midright: ")",
            botright: ")",
        };
    }

    // Linewrap
    let mut index = 0;
    if wrap {
        loop {
            if index + width >= s.len() {
                break;
            }

            let localwidth;
            let mut subindex = index + width;
            'b: loop {
                match (&s[index .. subindex]).ends_with(" ") {
                    true => {
                        localwidth = subindex - index;
                        break 'b;
                    }
                    false => {
                        subindex -= 1;
                    }
                }
            }
            let slice = &s[index .. index + localwidth];
            result.push(slice.to_string());
            index += localwidth;
        }
    }
    let slice = &s[index .. ];
    result.push(slice.to_string());

    // Bookend lines with bubble chars
    let mut longest = 0;
    let reslen = result.len() - 1;
    for (index, line) in result.iter_mut().enumerate() {
        match index {
            0   => match reslen {
                0 | 1 => *line = vec![cowb.sleft, line, cowb.sright].join(" "),
                _     => {
                    *line = vec![cowb.topleft, line, cowb.topright].join(" ")
                },
            },
            x if x < reslen => *line = vec![cowb.midleft, line, cowb.midright].join(" "),
            y if y == reslen => match reslen {
                1 => *line = vec![cowb.sleft, line, cowb.sright].join(" "),
                _ => *line = vec![cowb.botleft, line, cowb.botright].join(" ")
            },
            _ => panic!("Whoops!"),
        }
        if line.len() > longest {
            longest = line.len()
        }
    }

    // Pad to the longest line
    for line in &mut result {
        let mut padding = longest - line.len();
        let linelen = line.len();
        loop {
            match padding > 0 {
                false => break,
                true => {
                    line.insert(linelen - 1, pad);
                    padding -= 1;
                }
            };
        }
    }

    let mut top_bottom = longest - 2;
    loop {
        match top_bottom > 0 {
            false => break,
            true => {
                top.push(topc);
                bottom.push(bottomc);
                top_bottom -= 1;
            }
        }
    }
    result.insert(0, top.join(""));
    result.push(bottom.join(""));
    result.join("\n")
}

fn fortune() -> String {
    let fortunes: Vec<&str> = FORTUNES.split("%").collect();
    let mut rand_inx = rand::thread_rng();
    fortunes[rand_inx.gen_range(0..fortunes.len())].to_string().clone()
}

pub fn ovum() { 
    let width = 40;
    let wrap = true;
    let message = fortune();
    let tongue = " ";
    let eyes = "oo";
    let think = false;
    let voice = "\\";
    let cowbody = COW.to_string();
    
    println!("{}", make_bubble(message.to_string(), width, think, wrap));
    println!("{}", format_animal(cowbody, voice, eyes, tongue));
}
