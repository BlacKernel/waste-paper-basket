//! # Waste Paper Basket
//! 
//! ## What is it?
//! 
//! Waste Paper Basket (wpb) is a safe and sane implementation of GNU
//! coreutils `rm` command inspired by the idea behind [Trashy by
//! Klaatu](https://gitlab.com/trashy/trashy) and the code behind the
//! [uutils project's rust implementation of the GNU
//! coreutils](https://github.com/uutils/coreutils)
//! 
//! ## Why is it?
//! 
//! `rm` is both unsafe for someone who wants to keep data and someone who
//! wants to remove it. Waste Paper Basket aims to fix this by offering the
//! features and options of `rm`, while allowing it to both operate as a
//! sane trashcan as well as a safe way of removing files by passing it
//! through a process like `shred`
//! 
//! ## Why Rust?
//! 
//! I am using rust because it is sane, easily readable, and easily
//! maintainable. I encourage you to look at my code and the comments in it
//! to get a better feel for the language and programming in general.
//! 
//! The [tl;dr](https://tldr.sh) version of my stance on programming
//! languages, after using many in the last 20 years, is that they should be
//! easy to learn, easy to read, easy to understand, and,
//! <span id="most"></span>**most** importantly, should
//! <span id="never"></span>**never** abstract away the fact that you are
//! using a machine.
//! 
//! For more information on my views on rust, listen to my episode on HPR
//! [Rust 101 - Episode 0: What in
//! tarnishing?](https://hackerpublicradio.org/eps.php?id=3426).
//! 
//! # Current Features
//! 
//!   - <span class="done4"></span>- Trashes files
//!   - <span class="done4"></span>- Trashes empty directories (-d)
//!   - <span class="done4"></span>- Trashes directories recursively (-r)
//!   - <span class="done4"></span>- Can be forced to cooperate (-f)
//!   - <span class="done4"></span>- Can prompt always (-i), once (-I),
//!     never (default) or any WHEN you want (--interactive=WHEN)
//!   - <span class="done4"></span>- Passthrough to `rm` (-p)
//!   - <span class="done4"></span>- Undo trashings using regex on trashinfo
//!     files (-u)
//!   - <span class="done4"></span>- Empty trash can (-e)
//! 
//! # To-Do List
//! 
//!   - <span class="done0"></span>- Compatibility for all other `rm`
//!     flags\!
//!   - <span class="done0"></span>- Passthrough to `shred` (-s)
//!   - <span class="done0"></span>- Choose to use system trash (requires
//!     root) (--system)
//! 
//! ### Latest Release Versions
//! 
//! 
//! ### Contact Info
//! 
//! <span id="Email:"></span>**Email:** [izzy leibowitz at pm dot
//! me](izzyleibowitz@pm.me.html) <span id="Mastodon:"></span>**Mastodon:**
//! [at blackernel at nixnet dot social](https://nixnet.social/BlacKernel)

mod parse;
mod atomic_move;
mod clob_count;
mod trashinfo;
mod prompt;

mod passthrough;
mod list;
mod undo;
mod empty;
mod moove;
mod ovum;
mod trash;

use std::fs;
use std::path::Path;

fn main() {
    let options: parse::Options = parse::parse_options();
    let mut mut_options: parse::MutOptions = parse::parse_mut_options();


    if !Path::new(&options.dest_dir).exists() && !options.moove {
        match fs::create_dir(&options.dest_dir) {
            Ok(_)   => { if options.verbose { println!("created trash dir at {}", &options.dest_dir) } },
            Err(e)  => { if !options.force { eprintln!("could not create trash dir at {} -- {:?}", &options.dest_dir, e.kind()) }; return },
        }

        let directorysizes = [&options.dest_dir, "/directorysizes"].concat();
        match fs::write(&directorysizes, "") {
            Ok(_)   => { if options.verbose { println!("created directorysizes file at {}", &directorysizes) } },
            Err(e)  => { if !options.force { eprintln!("could not create directorysizes file at {} -- {:?}", &directorysizes, e.kind()) }; return },
        }
    }

    let trashfile_dir = [&options.dest_dir, "/files"].concat();
    if !Path::new(&trashfile_dir).exists() && !options.moove {
        match fs::create_dir(&trashfile_dir) {
            Ok(_)   => { if options.verbose { println!("created trash files dir at {}", &trashfile_dir) } },
            Err(e)  => { if !options.force { eprintln!("could not create trash files dir at {} -- {:?}", &trashfile_dir, e.kind()) }; return },
        }
    }

    let trashinfo_dir = [&options.dest_dir, "/info"].concat();
    if !Path::new(&trashinfo_dir).exists() && !options.moove {
        match fs::create_dir(&trashinfo_dir) {
            Ok(_)   => { if options.verbose { println!("created trash info dir at {}", &trashinfo_dir) } },
            Err(e)  => { if !options.force { eprintln!("could not create trash info dir at {} -- {:?}", &trashinfo_dir, e.kind()) }; return },
        }
    }

    //if options.shred        { shred::shred(options, mut_options) }
    /*else*/ if options.passthrough  { passthrough::passthrough(options, mut_options) }
    else if options.list    { list::list(options, mut_options) }
    else if options.undo    { undo::undo(options, mut_options) }
    else if options.empty   { empty::empty(options, mut_options) }
    else if options.moove   { moove::moove(options, mut_options) }
    else if options.ovum    { ovum::ovum() }
    else                    { trash::trash(options, mut_options) }
}
