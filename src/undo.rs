use crate::parse;
use crate::trashinfo::{read_trashinfo, TrashInfo};
use crate::clob_count::clob_count;
use crate::atomic_move::atomic_move;

use dialoguer::Select;

use std::fs;
use std::path::Path;

pub fn undo(options: parse::Options, mut mut_options: parse::MutOptions) {
    for file in &options.files {
        let file_path = Path::new(&file);
        let dest_dir  = Path::new(&options.dest_dir);

        let trashfile_dir = dest_dir.join("files");

        let ccount = clob_count(&file_path, &trashfile_dir, &options);

        let mut fixed_file = Path::new("");
        let mut fixed_file_name = String::new();

        if ccount == 0 {
            if !options.force { eprintln!("{} not found in trash\nplease run `wpb --list` to see the contents of your trash", file) }
            return
        } else if ccount == 1 {
            fixed_file = file_path
        } else {
            let file_name = file_path
                .file_name().unwrap()
                .to_str().unwrap();

            let mut matches: Vec<String> = Vec::new();
            let mut tmp_string = String::new();
            for i in 0..ccount {
                if trashfile_dir.join(&file_name).exists() || trashfile_dir.join(&format!("{}.{}", i, &file_name)).exists() {
                    if i == 0 { tmp_string = format!("{}", &file_name) }
                    else { tmp_string = format!("{}.{}", i, &file_name) }
                    matches.push(tmp_string);
                }
            }
            
            let selection = Select::new()
                .with_prompt("use <SP> to select which file you wish to undo")
                .items(&matches)
                .interact()
                .unwrap();

            fixed_file_name = matches[selection].clone();
            fixed_file = Path::new(&fixed_file_name);
        }
        
        let trashinfo_dir = dest_dir.join("info");

        if options.verbose { println!("attempting to read {}/{}.trashinfo", trashinfo_dir.display(), fixed_file.display()) }

        let trashinfo: TrashInfo = match read_trashinfo(&fixed_file, &trashinfo_dir) {
            Ok(t)   => { if options.verbose { println!("successfully read trashinfo file") }; t},
            Err(e)  => { if !options.force { eprintln!("failed to read trashinfo file -- {:?}", e.kind()) }; continue }
        };
        let trashinfo_path = Path::new(&trashinfo.path);

        if  trashinfo_path.exists() && !options.force {
            eprintln!("chose not to clobber {}\nfailed to undo", trashinfo_path.display());
            continue
        }

        mut_options.dest_dir = trashinfo_path.parent().unwrap().display().to_string();

        let dest = [&trashfile_dir.display().to_string().clone(), "/", fixed_file.file_name().unwrap().to_str().unwrap()].concat();
        let dest_path = Path::new(&dest);

        if dest_path.is_dir() {
            // TODO: Remove entry from directorysizes
        }

        match atomic_move(&dest_path, &trashinfo_path, &options, &mut mut_options) {
            false   => { if options.verbose { println!("{} undone successfully", file) } },
            true    => { if !options.force { eprintln!("failed to undo {}", file) }; continue },
        }

        let trashinfo_file_name =  format!("{}.trashinfo", fixed_file.file_name().unwrap().to_str().unwrap());
        let trashinfo_file = trashinfo_dir.join(&trashinfo_file_name);

        match fs::remove_file(trashinfo_file) {
            Ok(_)   => { if options.verbose { println!("removed trashinfo file successfully") } },
            Err(e)  => { if !options.force { eprintln!("failed to remove trashinfo file") }; continue },
        }
    }
}
