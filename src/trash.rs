use crate::parse;
use crate::atomic_move::atomic_move;
use crate::clob_count::clob_count;
use crate::trashinfo::make_trashinfo;
use crate::prompt::prompt;

use std::path::{Path, PathBuf};

pub fn trash(options: parse::Options, mut mut_options: parse::MutOptions) {

    if options.interactive == parse::InteractiveMode::Once && (options.recursive || options.files.len() >= 3) && !options.force {
        if !prompt("are you sure you wish to trash these files recursively?") {
            return
        }
    }

    for file in &options.files {
        let file_path = Path::new(&file);
        let dest_dir  = Path::new(&options.dest_dir);

        if file_path.parent() == None {
            if !options.force { eprintln!("refusing to impossibly trash the root directory") }
            continue
        }

        let trashfile_dir = dest_dir.join("files");

        let ccount = clob_count(&file_path, &trashfile_dir, &options);

        let mut fixed_file = Path::new("");
        let mut fixed_file_pathbuf = PathBuf::new();

        if ccount == 0 { fixed_file = file_path }
        else
        {
            let str_ccount = format!("{}", ccount);
            let fixed_file_name = [ str_ccount, ".".to_string(), file.to_string() ].concat();

            fixed_file_pathbuf = dest_dir.join(&fixed_file_name);
            fixed_file = &fixed_file_pathbuf;
        }


        if options.verbose { println!("attempting to create trashinfo for {}", file) }

        let trashinfo_dir = dest_dir.join("info");

        match make_trashinfo(&file_path, &fixed_file, &trashinfo_dir, &options) {
            true  => { if !options.force { eprintln!("failed to create trashinfo") }; std::process::exit(1) },
            false => { if options.verbose { println!("created trashinfo") } },
        }

        if options.verbose { println!("attempting to trash {}", file) }

        
        mut_options.dest_dir = trashfile_dir.display().to_string();

        let dest = [&mut_options.dest_dir.clone(), "/", fixed_file.file_name().unwrap().to_str().unwrap()].concat();
        let dest_path = Path::new(&dest);

        match atomic_move(&file_path, &dest_path, &options, &mut mut_options) {
            true  => { if !options.force { eprintln!("failed to trash {}", file) } },
            false => { if options.verbose { println!("trashed {} successfully", file) } },
        }
    }
}
