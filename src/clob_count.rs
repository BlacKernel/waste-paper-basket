use std::fs;
use std::path::Path;

use regex::Regex;

use crate::parse;

pub fn clob_count(from: &Path, to: &Path, options: &parse::Options) -> usize {
    let mut clob_count: usize = 0; // This will be the return value (How many times we'll clobber)

    if !to.is_dir() {
        if !options.force { eprintln!("ERROR: Could not check file clobbering from {} into regular file {}\nCANNOT CONTINUE", from.display(), to.display()) }
        std::process::exit(127)
    }

    if to.join( from.file_name().unwrap() ).exists()
    { clob_count = 1 }

    let file_stem = from
        .file_stem()
        .unwrap()
        .to_str()
        .unwrap();

    let path_pat = Regex::new(
        &[r"\.?(\d*)\.", file_stem, "$"].concat()
        ).unwrap();

    for entry in fs::read_dir(to).unwrap() {
        let entry = entry.unwrap();

        if let Some(cap) = path_pat.captures( entry.file_name().to_str().unwrap() )
        { clob_count = &cap[1].parse::<usize>().unwrap_or(0) + 1 }

    }

    clob_count
}
