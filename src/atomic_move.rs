use crate::parse;
use crate::prompt::prompt;

use std::path::Path;
use std::fs;

pub fn atomic_move(file: &Path, dest_dir: &Path, options: &parse::Options, mut_options: &mut parse::MutOptions) -> bool {

        let metadata = match fs::symlink_metadata(file) {
            Ok(m)   => { if options.verbose { println!("...{}ing {}: read metadata of {1}", options.mode_message, file.display()) }; m },
            Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not read metadata -- {:?}", options.mode_message, file.display(), e.kind()) }; return true }
        };

        if metadata.is_file() {

            if options.interactive == parse::InteractiveMode::Always && !options.force {
                let msg = format!("are you sure you want to {} regular file {}?", options.mode_message, file.display());
                if !prompt(&msg) {
                    return true
                }
            }

            // If we aren't emulating rm, copy the file
            if !options.passthrough {
                match fs::copy(file, dest_dir) {
                    Ok(_)   =>  if options.verbose { println!("...{}ing {}: copied {1} to {}", options.mode_message, file.display(), dest_dir.display()) },
                    Err(e)  =>  { if !options.force { eprintln!("...could not {} {}: could not copy {1} to {} -- {:?}", options.mode_message, file.display(), dest_dir.display(), e.kind()) }; return true }
                }
            }

            // Either way, delete the original file
            match fs::remove_file(file) {
                Ok(_)   => if options.verbose { println!("...{}ing {}: removed original regular file {1}", options.mode_message, file.display()) },
                Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not remove original regular file {1} -- {:?}", options.mode_message, file.display(), e.kind()) }; return true }
            }
        } else if metadata.is_dir() {

            if is_empty(file) && (options.dir || options.recursive) {

                if options.interactive == parse::InteractiveMode::Always && !options.force {
                    let msg = format!("are you sure you wish to {} empty directory {}?", options.mode_message, file.display());
                    if !prompt(&msg) {
                        return true
                    }
                }

                
                // If we aren't emulating rm, make a new empty directory at the destination
                if !options.passthrough {
                    match fs::create_dir(dest_dir) {
                        Ok(_)   => if options.verbose { println!("...{}ing {}: created new empty directory at {}", options.mode_message, file.display(), dest_dir.display()) },
                        Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not create new empty directory at {} -- {:?}", options.mode_message, file.display(), dest_dir.display(), e.kind()) }; return true },
                    }
                }

                // Either way, remove the original directory
                match fs::remove_dir(file) {
                    Ok(_)   => if options.verbose { println!("...{}ing {}: removed original empty directory", options.mode_message, file.display()) },
                    Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not remove original empty directory -- {:?}", options.mode_message, file.display(), e.kind()) }; return true },
                }
            } else if !is_empty(file) && options.recursive {

                if options.interactive == parse::InteractiveMode::Always && !options.force {
                    let msg = format!("are you sure you wish to decend into {}?", file.display());
                    if !prompt(&msg) {
                        return true
                    }
                }
                
                // If we aren't emulating rm, make a new empty directory at the destination
                if !options.passthrough {
                    match fs::create_dir(dest_dir) {
                        Ok(_)   => if options.verbose { println!("...{}ing {}: created new empty directory at {}", options.mode_message, file.display(), dest_dir.display()) },
                        Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not create new empty directory at {} -- {:?}", options.mode_message, file.display(), dest_dir.display(), e.kind()) }; return true },
                    }
                }

                recurse(file, dest_dir, options, mut_options);

                // Either way, remove the original directory
                match fs::remove_dir(file) {
                    Ok(_)   => if options.verbose { println!("...{}ing {}: removed original directory", options.mode_message, file.display()) },
                    Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not remove original directory -- {:?}", options.mode_message, file.display(), e.kind()) }; return true },
                }

            } else {
                if !options.force {
                    eprintln!("...could not {} {}: can't move directory, use --directory or --recursive", options.mode_message, file.display());
                    return true
                }
            }

        } else if let Ok(link) = fs::read_link(file) {

            if options.interactive == parse::InteractiveMode::Always && !options.force{
                let msg = format!("are you sure you wish to {} symlink {}?", options.mode_message, file.display());
                if !prompt(&msg) {
                    return true
                }
            }

            // If we aren't emulating rm, make a new link pointing to the same file in the destination in the dest_dir
            if !options.passthrough {
                match std::os::unix::fs::symlink(link, dest_dir) {
                    Ok(_)   => if options.verbose { println!("...{}ing {}: created new symbolic link {} pointing to the original link location", options.mode_message, file.display(), dest_dir.display()) },
                    Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not create new symbolic link {} pointing to the original link location -- {:?}", options.mode_message, file.display(), dest_dir.display(), e.kind()) }; return true }
                }
            }

            // Either way, delete the original symlink
            match fs::remove_file(file) {
                Ok(_)   => if options.verbose { println!("...{}ing {}: removed original symlink {1}", options.mode_message, file.display()) },
                Err(e)  => { if !options.force { eprintln!("...could not {} {}: could not remove original symlink {1} -- {:?}", options.mode_message, file.display(), e.kind()) }; return true }
            }

        } else {

            // Error: File not file, dir, nor symlink
            if !options.force {
                eprintln!("...could not {} {}: not file, dir, nor symlink", options.mode_message, file.display())
            }
            return true
        }

    false
}

pub fn is_empty(file: &Path) -> bool {
    if !file.is_dir() { return false }
    if file.read_dir().map(|mut i| i.next().is_none()).unwrap_or(false) { return true }
    false
}

pub fn recurse(src_file: &Path, dest_file: &Path, options: &parse::Options, mut_options: &mut parse::MutOptions) -> bool {
    if options.verbose { println!("recursing into {}...", src_file.display()) }

    let dest_file_name = dest_file.file_name().unwrap().to_str().unwrap().to_string();
    let dest_suffix = format!("/{}", dest_file_name);
    mut_options.recur_dest = [mut_options.recur_dest.clone(), dest_suffix.clone()].concat();

    for entry in fs::read_dir(src_file).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();

        let dest = [mut_options.dest_dir.clone(), mut_options.recur_dest.clone(), "/".to_string(), path.file_name().unwrap().to_str().unwrap().to_string()].concat();
        let dest_path = Path::new(&dest);

        if options.verbose { println!("{}ing {}...", options.mode_message, &path.display()) }
        match atomic_move(&path, &dest_path, options, mut_options) {
            true  => { if !options.force { eprintln!("could not {} {}: move failed", options.mode_message, &path.display()) }; return true },
            false => if options.verbose { println!("{}ed {} successfully", options.mode_message, &path.display()) },
        }
    }

    mut_options.recur_dest = mut_options.recur_dest.trim_end_matches(&dest_suffix).to_string();

    false

}
