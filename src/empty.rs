use std::path::Path;
use std::fs;

use crate::parse;
use crate::prompt::prompt;

pub fn empty(options: parse::Options, mut_options: parse::MutOptions) {

    if !prompt(&["are you sure you wish to empty the trash at ", &options.dest_dir, "?"].concat()) {
        return
    }

    if options.verbose { println!("emptying {}", &options.dest_dir) }

    match empty_trash_files(&options, &mut_options) {
        false   => { if options.verbose { println!("...emptying {}: removed trashed files", &options.dest_dir) } },
        true    => { if !options.force { eprintln!("failed to empty {}: could not remove trashed files", &options.dest_dir) }; return },
    }

    match empty_trash_info(&options, &mut_options) {
        false   => { if options.verbose { println!("...emptying {}: removed trashinfo files", &options.dest_dir) } },
        true    => { if !options.force { eprintln!("failed to empty {}: could not remove trashinfo files", &options.dest_dir) }; return },
    }

    match empty_directorysizes(&options, &mut_options) {
        false   => { if options.verbose { println!("...emptying {}: cleared directorysizes", &options.dest_dir) } },
        true    => { if !options.force { println!("failed to empty {}: could not clear directorysizes file", &options.dest_dir) }; return },
    }

    if options.verbose { println!("emptied {} successfully", &options.dest_dir) }
}

fn empty_trash_files(options: &parse::Options, mut_options: &parse::MutOptions) -> bool {
    let dest_dir = Path::new(&options.dest_dir);
    let trashfile_dir = dest_dir.join("files");

    for entry in fs::read_dir(trashfile_dir).unwrap() {
        let entry = entry.unwrap();
        let path  = entry.path();

        if path.is_dir() {
            match fs::remove_dir_all(path) {
                Ok(_)   => {},
                Err(e)  => { return true },
            }
        } else {
            match fs::remove_file(path) {
                Ok(_)   => {},
                Err(e)  => { return true },
            }
        }
    }
    false
}

fn empty_trash_info(options: &parse::Options, mut_options: &parse::MutOptions) -> bool {
    let dest_dir = Path::new(&options.dest_dir);
    let trashinfo_dir = dest_dir.join("info");

    for entry in fs::read_dir(trashinfo_dir).unwrap() {
        let entry = entry.unwrap();
        let path  = entry.path();

        if !path.is_dir() {
            match fs::remove_file(path) {
                Ok(_)   => {},
                Err(e)  => { return true }
            }
        }
    }

    false
}

fn empty_directorysizes(options: &parse::Options, mut_options: &parse::MutOptions) -> bool {
    let dest_dir = Path::new(&options.dest_dir);
    let directorysizes = dest_dir.join("directorysizes");

    match fs::write(directorysizes, "") {
        Ok(_)   => {},
        Err(e)  => { return true },
    }

    false
}
