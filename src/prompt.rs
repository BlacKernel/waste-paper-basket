use dialoguer::Confirm;

pub fn prompt(msg: &str) -> bool {
    if Confirm::new().with_prompt(msg).interact().unwrap() {
        return true
    } else {
        return false
    }
}
