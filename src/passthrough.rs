use crate::parse;
use crate::atomic_move::atomic_move;

use std::path::Path;

pub fn passthrough(options: parse::Options, mut mut_options: parse::MutOptions) {
    for file in &options.files {
        let file_path = Path::new(&file);
        let dev_null = Path::new("/dev/null"); // NOTE: This is a joke

        if file_path.parent() == None && options.preserve_root {
            if !options.force { eprintln!("refusing to remove root directory") }
            continue
        }

        if options.verbose { println!("attempting to remove {}", file) }

        match atomic_move(&file_path, &dev_null, &options, &mut mut_options) {
            false   => { if options.verbose { println!("removed {} successfully", file) } },
            true    => { if !options.force { eprintln!("failed to remove {}", file) } },
        }
    }
}
